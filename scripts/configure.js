#!/usr/bin/env node
"use strict";
require('dotenv').config();
const fs = require("fs");
const exec = require("child_process").execSync;
const modifyFiles = require("./utils").modifyFiles;

let minimistHasBeenInstalled = false;

if (!fs.existsSync("./node_modules/minimist")) {
  exec("yarn add minimist");
  minimistHasBeenInstalled = true;
}

const args = require("minimist")(process.argv.slice(2), {
  string: ["access-key-id", "secret-access-key", "account-id", "bucket-name", "function-name", "region"]
});

if (minimistHasBeenInstalled) {
  exec("npm uninstall minimist --silent");
}

const accountId = process.env.AWS_ACCOUNT_ID || args["account-id"];
const accessKeyId = process.env.AWS_ACCESS_KEY_ID || args["access-key-id"];
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY || args["secret-access-key"];
const region = process.env.AWS_REGION || args.region;
const bucketName = process.env.AWS_BUCKET_NAME || args["bucket-name"];
const functionName = process.env.AWS_SERVERLESS_EXPRESS_LAMBDA_FUNCTION_NAME || args["function-name"];
const cloudFormationStackName = process.env.AWS_CLOUD_FORMATION_STACK_NAME || args["cf-stack-name"];

if (!accountId || accountId.length !== 12) {
  console.error(
    'You might supply a 12 digit account id as --account-id="<accountId>"'
  );
  process.exit(1);
}

if (!accessKeyId) {
  console.error(
    'You might supply a bucket name as --access-key-id="<accessKeyId>"'
  );
  process.exit(1);
}

if (!secretAccessKey) {
  console.error(
    'You might supply a bucket name as --secret-access-key="<secretAccessKey>"'
  );
  process.exit(1);
}

if (!bucketName) {
  console.error(
    'You might supply a bucket name as --bucket-name="<bucketName>"'
  );
  process.exit(1);
}

if (!functionName) {
  console.error(
    'You might supply a function name as --function-name="<functionName>"'
  );
  process.exit(1);
}

if (!cloudFormationStackName) {
  console.error(
    'You might supply a cloudformation stack name as --cf-stack-name="<cloudFormationStackName>"'
  );
  process.exit(1);
}

modifyFiles(
  [
    "./simple-proxy-api.yaml",
    "./package.json",
    "./cloudformation.yaml"
  ],
  [
    {
      regexp: /YOUR_ACCOUNT_ID/g,
      replacement: accountId
    },
    {
      regexp: /YOUR_ACCESS_KEY_ID/g,
      replacement: accessKeyId
    },
    {
      regexp: /YOUR_SECRET_ACCESS_KEY/g,
      replacement: secretAccessKey
    },
    {
      regexp: /YOUR_AWS_REGION/g,
      replacement: region
    },
    {
      regexp: /YOUR_UNIQUE_BUCKET_NAME/g,
      replacement: bucketName
    },
    {
      regexp: /YOUR_SERVERLESS_EXPRESS_LAMBDA_FUNCTION_NAME/g,
      replacement: functionName
    },
    {
      regexp: /YOUR_CLOUD_FORMATION_STACK_NAME/g,
      replacement: cloudFormationStackName
    }
  ]
);
