const fs = require('fs');
const path = require('path');

function packageCode() {
  try {
    fs.copyFileSync('./.env', './dist/.env');
    fs.copyFileSync('./firebase-service-account.json', './dist/firebase-service-account.json');
    fs.copyFileSync('./lambda.js', './dist/lambda.js');
    fs.copyFileSync('./package.json', './dist/package.json');
    copyDir('./build', './dist/build');
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}
function copy(src, dest) {
	const oldFile = fs.createReadStream(src);
	const newFile = fs.createWriteStream(dest);
	oldFile.pipe(newFile);
}
function copyDir(src, dest) {
  try {
    fs.mkdirSync(dest);
    const files = fs.readdirSync(src);
    for(var i = 0; i < files.length; i++) {
      const current = fs.lstatSync(path.join(src, files[i]));
      if(current.isDirectory()) {
        copyDir(path.join(src, files[i]), path.join(dest, files[i]));
      } else if(current.isSymbolicLink()) {
        const symlink = fs.readlinkSync(path.join(src, files[i]));
        fs.symlinkSync(symlink, path.join(dest, files[i]));
      } else {
        copy(path.join(src, files[i]), path.join(dest, files[i]));
      }
    }
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

fs.exists('./dist', function(exists) {
  if(exists) {
    packageCode();
  } else {
    fs.mkdir('./dist', function (err) {
      if (err){
        console.error(err);
        process.exit(1);
        return;
      }
      packageCode();
    });
  }
});