import express from "express";
import { usersRoute } from "features/users";
import { createPostRoute } from "features/posts";
import { createNotificationRouter } from "features/notification";
import { createProfanityRouter } from "features/profanity";
import { advertiseRouter } from "features/advertise/ad.router";
export function apiRoutes() {
  const router = express.Router();

  router.get("/check", (req, res) => {
    res.json({ isAlive: true });
  });
  router.use("/users", usersRoute());
  router.use("/posts", createPostRoute());
  router.use("/notifications", createNotificationRouter());
  router.use("/profanities", createProfanityRouter());
  router.use("/advertise", advertiseRouter());
  return router;
}
