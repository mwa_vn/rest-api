process.env.NODE_ENV = "local";
process.env.WEB_HOSTNAME = "localhost:3000";
import app from "./app";

const port = 3001;

app.listen(port, function() {
  console.log(`listening on http://localhost:${port}`);
});
