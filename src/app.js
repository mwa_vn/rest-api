import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import compression from "compression";
import awsServerlessExpressMiddleware from "aws-serverless-express/middleware";
import { apiRoutes } from "./routes";
import mongoose from "mongoose";
import apiConfig from "api-config";
import { authenticatedMiddleware } from "features/users/user.controller";

const app = express();

mongoose.Promise = global.Promise;
mongoose
  .connect(apiConfig.mongoUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });

app.use(compression());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(authenticatedMiddleware());
app.use(awsServerlessExpressMiddleware.eventContext());
app.use("/api", apiRoutes());

function getResponse (message) {
  return {
    error: message
  };
}
function handleGlobalError(err, req, res, next) {
	if (err && err.name === 'UnauthorizedError') {
		console.error(err.stack);//this error will be logged into cloudwatch if it's run on cloud otherwise will be printed in console
		res.status(401).send(getResponse(err.message));
	} else if (err) {
		console.error(err.stack);//this error will be logged into cloudwatch if it's run on cloud otherwise will be printed in console
		res.status(500).send(getResponse(err.message));
	} else {
		next();
	}
}
app.use(handleGlobalError)


// cron.schedule("* * * * *", function() {
//   console.log("---------------------");
//   console.log("Running Cron Job Profanity - every minute");
//   syncFilter();
// });

// Export your express server so you can import it in the lambda function.
module.exports = app;
export default app;
