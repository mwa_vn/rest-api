import express from "express";
import {
    authenticate,
    create,
    getAll,
    getById,
    update,
    remove,
    getProfile,
    follow,
    getFollowers,
    getFollowings,
    getPeople,
    submitActiveRequest,
    activate, getInActiveUsers
} from "./user.controller";
import UserService from "services/user.services";

export function usersRoute() {
    const router = express.Router();

    // get user.id from jwt
    router.put("/follow/:personId/:stt", follow);
    router.get("/followers", getFollowers);
    router.get("/followings", getFollowings);

    router.get("", getAll);
    router.get("/people", getPeople);
    router.get("/:id", getById);
    router.get("/profile/:id", getProfile);
    router.get("/list/inactive", getInActiveUsers);

    router.post("/submitActiveRequest", submitActiveRequest);
    router.put("/:userId/activate", activate);
    router.post("/authenticate", authenticate);
    router.post("/register", create);
    router.post("", create);

    router.put("/:id", update);

    router.delete("/:id", remove);

    router.post("/:id/upload-images", (req, res, next) => {
        UserService.uploadImage(req)
            .then(fileData => res.json(fileData))
            .catch(error => {
                console.log(error);
                next(error);
            });
    });

    return router;
}
