import jwt from 'jsonwebtoken';
import expressJwt from 'express-jwt';
import mongoose from "mongoose";
import userSchema from "./user.model";
import apiConfig from 'api-config';
import NotificationService from 'services/notification';
import crypto from 'crypto';
import omit from 'lodash/fp/omit';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/fp/get';

const User = mongoose.model("User", userSchema);

const PATHS_WITH_OPEN_ACCESS = [
  '/api/users/authenticate',
  '/api/users/register',
  '/api/notifications',
  '/api/notifications/test-email'
  // /\/api\/advertise./g
];

export function authenticatedMiddleware() {
    return expressJwt({
        secret: apiConfig.jwtSecretKey
    }).unless({ path: PATHS_WITH_OPEN_ACCESS });
}

export function authenticate(req, res, next) {
    const { username, password } = req.body;
    //1.encrypt password before query and compare
    //2. query user by username(email)
    //3. get salt and encrypt req.password
    //const hash = crypto.pbkdf2Sync
    //4. compare and return

    const match = {
        email: { $eq: username }
        //password: { $eq: password }
    }
    User.findOne(match, (error, user) => {
        if (error) {
            return next(error);
        }

        if (user) {
            if (!user.validPassword(password)) {
                res.send({ error: 'username or password does not match' });
            }
            const $user = user.toJSON();
            const payload = {
                id: $user.id,
                username,
                role: $user.role || 'USER'
            }
            const jwtOptions = {
                expiresIn: apiConfig.jwtExpiresIn * 60 * 60
            };
            jwt.sign(payload, apiConfig.jwtSecretKey, jwtOptions, (err, token) => {
                if (err) {
                    next(err);
                } else {
                    res.send({
                        id: $user.id,
                        username,
                        role: $user.role,
                        token,
                        token_type: 'Bearer'
                    });
                }
            });
        } else {
            res.send({ error: 'username or password does not match' });
        }
    });
}

export function create(req, res) {
    User.findOne({ email: req.body.email }, (error, user) => {
        if (user) {
            res.send({ error: 'email was used' });
        } else {
            let user = new User(req.body);
            //encrypt password here
            user.setPassword(req.body.password);
            user
                .save()
                .then(data => {
                    res.send(data);
                })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating the User."
                    });
                });
        }
    });

}

export async function getAll(req, res) {

    const match = {};
    const sort = {};

    if (req.query.name) {
        match.name = { $regex: new RegExp(req.query.name), $options: "i" };
    }

    if (req.query.sortBy && req.query.OrderBy) {
        sort[req.query.sortBy] = req.query.OrderBy === "desc" ? -1 : 1;
    }

    try {
        const limit = parseInt(req.query.limit) || 5; // results per page
        const page = parseInt(req.query.page) || 1; // Page
        const foundUsers = await User.find(match)
            .skip(limit * page - limit)
            .limit(limit)
            .sort(sort);

        const numberUser = await User.count(match);

        res.send({
            data: foundUsers,
            currentPage: page,
            pages: Math.ceil(numberUser / limit),
            match: { name: req.query.name },
            sort: sort,
            count: numberUser,
            limit: limit
        });
    } catch (err) {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving User."
        });
    }
}

export function getById(req, res) {
    const id = req.params.id;

    User.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found User with id " + id });
            else res.send(data);
        })
        .catch(() => {
            res.status(500).send({ message: "Error retrieving User with id=" + id });
        });
}

export function getProfile(req, res) {
    const id = req.params.id;

    User.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found User with id " + id });
            else res.send(data.toObject({ virtuals: true }));
        })
        .catch(() => {
            res.status(500).send({ message: "Error retrieving User with id=" + id });
        });
}

export function update(req, res) {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    User.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update User with id=${id}. Maybe User dmin was not found!`
                });
            } else res.send({ message: "User was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating User with id=" + id
            });
        });
}

export function remove(req, res) {
    const id = req.params.id;

    User.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete User with id=${id}. Maybe User was not found!`
                });
            } else {
                res.send({
                    message: "User was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete User with id=" + id
            });
        });
}

export async function follow(req, res) {

    try {
        let user = await User.findById(req.user.id);
        if (user == undefined) throw Error('User is not found!');

        let person = await User.findById(req.params.personId);
        if (person == undefined) throw Error('Person who you want to (un)follow is not found!');

        let updateInfo = req.params.stt === 'true' ? { $addToSet: { following: person._id } } : { $pull: { following: person._id } };

        await User.findByIdAndUpdate(req.user.id, updateInfo, { useFindAndModify: false });

        res.send({ message: '(Un)follow is successfully' });
    } catch (err) {
        res.send({ message: err });
    }
}

export async function getFollowers(req, res) {

    try {
        let user = await User.findById(req.user.id);
        if (user == undefined) throw Error('User is not found!');

        let followers = await User.find({ following: user._id });

        res.send(followers);
    } catch (err) {
        res.send({ message: err });
    }
}

export async function getFollowings(req, res) {

    try {
        let user = await User.findById(req.user.id)
            .populate('following');
        if (user == undefined) throw Error('User is not found!');

        res.send(user.following);
    } catch (err) {
        res.send({ message: err });
    }
}

export async function getPeople(req, res) {
    try {
        let crrUser = await User.findById(req.user.id);
        const { lastId = '', search = '', category = 'all' } = req.query;
        const query = {
            disabled: false,
            _id: { $ne: req.user.id }
        };

        if (!isEmpty(search)) {
            query['$or'] = [
                { "userProfile.firstname": { $regex: "^" + search + "$", $options: 'i' } },
                { "userProfile.lastname": { $regex: "^" + search + "$", $options: 'i' } }
            ];
        }

        query._id = {$ne: req.user.id};
        if (category === 'following') {
            query._id = { $in: crrUser.following, ...query._id };
        } else if (category === 'unfollowing') {
            query._id = { $nin: crrUser.following, ...query._id };
        } else if (category === 'follow_me') { // people follow signed in user
            query.following = { $in: [req.user.id] };
        }

        if (!isEmpty(lastId)) {
            query._id = {$gt: lastId, ...query._id };
        }
        //console.log(query);
        let users = await User.find(query).limit(Number(10));
        users = users.map(e => {
            const {
                _id,
                userProfile,
                ...object
            } = e.toObject();
            object.id = _id;
            object.name = userProfile.firstname + ' ' + userProfile.lastname;
            object.photo = userProfile.photo != undefined ? userProfile.photo : 'https://i.pravatar.cc/300';
            object.followed = crrUser.following.includes(_id);
            object.myFollower = e.following.includes(crrUser._id);
            return omit(['__v', 'salt', 'hash', 'following', 'role', 'password', 'dob', 'disabled', 'unhealthyPost', 'zipcode', 'createdAt', 'updatedAt'])(object);
        });

        res.send(users);
    } catch (err) {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving User."
        });
    }
}

export async function submitActiveRequest(req, res) {
    const { explaination } = req.body;
    const userId = req.user.id;
    const user = await User.findById(userId);
    if (user) {
        const admins = await User.find({ role: 'ADMIN' });
        const adminIds = admins.map(u => u.id);
        const notification = {
            title: `${get('userProfile.firstname')(user)} ${get('userProfile.lastname')(user)} submitted an active request.`,
            body: explaination
        }
        try {
            await NotificationService.sendActiveRequestNotificationForAdmin(adminIds, notification, userId);
        } catch {
            // ignore error
        }
        res.sendStatus(204);
    } else {
        res.send({ error: 'User does not exist!!!' });
    }
}

export async function activate(req, res) {
    const userId = req.params.userId;
    try {
        await User.findByIdAndUpdate(userId, { disabled: req.body.disabled });
        res.sendStatus(204);
    } catch (error) {
        res.send({ error: 'Cannot activate user because there is an error occured during processing.' });
    }
}

export async function getInActiveUsers(req, res) {
    User.find({ disabled: true })
        .then(data => {
            res.send(data);
        })
        .catch(() => {
            res.status(500).send({ message: "Error retrieving in-active User" });
        });
}