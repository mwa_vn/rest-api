import mongoose from "mongoose";
import validator from "validator";
import crypto from "crypto";

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: {
        type: String,
        required: "Email required",
        unique: true,
        trim: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Email is invalid!");
            }
        }
    }, //TODO: encode when update with jwt

    hash: String,
    salt: String,
    zipcode: {
        type: Number,
        required: "Zipcode is required."
    },
    dob: {
        type: Date,
        required: "Birthday is required."
    },
    disabled: { type: Boolean, required: true, default: false },
    role: { type: String, enum: ['ADMIN', 'USER'], default: 'USER' },
    userProfile: {
        firstname: {
            type: String,
            required: "First name is required."
        },
        lastname: {
            type: String,
            required: "Last name is required."
        },
        gender: {
            type: String,
            required: "Gender name is required."
        },
        photo: String,
        school: String,
        work: String,
        phone: String
    },
    unhealthyPost: { type: Number, default: 0 },
    following: [{ type: Schema.Types.ObjectId, ref: 'User' }] // users who you follow
        //follower: [{type: Schema.Types.ObjectId, ref: 'User'}] //you can query your followers
}, { timestamps: true }, {
    toObject: {
        virtuals: true
    }
});

UserSchema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});

UserSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};


UserSchema.methods.validPassword = function(password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

UserSchema.virtual('fullName').get(function() { return this.userProfile.firstname + ' ' + this.userProfile.lastname; })

export default UserSchema;