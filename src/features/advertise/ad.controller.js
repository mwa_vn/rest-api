import mongoose from "mongoose";
import adSchema from "./ad.model";
import userSchema from "../users/user.model"
import apiResponse from "./ApiResponse";
import AssetsService from "../../services/assets";
import { objectdisplay, builtQuery } from "./ad.common";
import ad from "./ad.model";
const adModel = mongoose.model("advertise", adSchema);
const userModel = mongoose.model("User", userSchema);
export async function create(req, res) {

    //const buffer = fs.readFileSync(path.join(__dirname,'../../../public/images/noimage.png'));
    //servelet doesn't support this path so turn this image into b64String.       

    //save image to database
    // var buffer = Buffer.from(image64.b64string, 'base64');
    // req.body.image = buffer;        

    console.log('data', req.body);
    //save image by link
    req.body.imageUrl = `https://d2uxsr5p1qtf3u.cloudfront.net/advertise/5e95446bcd2250536886ce0c/noimage.png`;
    req.body.createdOn = Date.now();
    userModel.aggregate(builtQuery(req)).exec((err, data) => {
        if (err) res.status(500).send(new apiResponse(500, "error", err));


        if (data.map(a => a._id).length != 0)
            req.body.receives = data.map(a => a._id);


        adModel(req.body).save().then((data) => {
            adModel.findById(data._id).select(objectdisplay).then(data => {
                res.status(200).send(new apiResponse(200, "success", data));
            }).catch(err => {
                res.status(500).send(new apiResponse(500, "error", err));
            });

        }).catch(err => {
            res.status(500).send(new apiResponse(500, "error", err));
        });
    });

};

export async function getAll(req, res) {

    try {
        const data = await adModel.find({});
        res.status(200).send(new apiResponse(200, "success", data));
    } catch (err) {
        res.status(500).send(new apiResponse(500, "error", err));
    }
}
export async function paging(req, res) {


    let search = undefined;
    if (req.query.type != "undefined")//homepage 
    {
        getRandom(req,res);  
        return;
    }
        
    else
        search = req.query.search == "undefined" ? {} : { name: { '$regex': req.query.search, '$options': 'i' } };

    let sortArray = req.params["sort"].split("=");//fieldName=asc/desc
    const fieldName = sortArray[0];
    const asc = sortArray[1] == "asc" ? 1 : -1;
    let sort = {}; sort[fieldName] = asc;
    let pagesize = +req.params["pagesize"];
    let page = +req.params["page"] * pagesize;

    //const data = await adModel.find({}).skip(page).limit(pagesize).sort(sort); //+ convert to number
    adModel.aggregate([{ $match: search }, { $sort: sort }, { $skip: page }, { $limit: pagesize }, { $project: { name: "$name", description: "$description", createdOn: "$createdOn", imageUrl: "$imageUrl", linkUrl: "$linkUrl" } }]).exec((err, datafilter) => {
        if (err) res.status(500).send(new apiResponse(500, "error", err));
        else {
            adModel.aggregate([{ $match: search }]).exec((err, data) => {
                if (err) res.status(500).send(new apiResponse(500, "error", err));
                else {
                    res.set("Access-Control-Expose-Headers", "*");
                    // res.set("Access-Control-Expose-Headers","X-Row-Header");                       
                    res.set("X-Paging-Header", Math.ceil(data.length / pagesize));
                    res.set("X-Row-Header", Math.ceil(data.length));
                    res.status(200).send(new apiResponse(200, "success", datafilter));
                }
            });
        }
    });

}
export async function getRandom(req,res){

    let search = { $or: [{ receives: { $exists: false } }, { receives: { $in: [mongoose.Types.ObjectId(req.user.id)] } }] }; //,{receives:{$eq:[]}}
   
    let sortArray = req.params["sort"].split("=");//fieldName=asc/desc
    const fieldName = sortArray[0];
    const asc = sortArray[1] == "asc" ? 1 : -1;
    let sort = {}; sort[fieldName] = asc;
    let pagesize = +req.params["pagesize"];
    let page = +req.params["page"] * pagesize;

    //const data = await adModel.find({}).skip(page).limit(pagesize).sort(sort); //+ convert to number
    adModel.aggregate([{ $match: search }, { $sort: sort },{$sample:{size:pagesize}} , { $project: { name: "$name", description: "$description", createdOn: "$createdOn", imageUrl: "$imageUrl", linkUrl: "$linkUrl" } }]).exec((err, datafilter) => {
        if (err) res.status(500).send(new apiResponse(500, "error", err));
        else {
            adModel.aggregate([{ $match: search }]).exec((err, data) => {
                if (err) res.status(500).send(new apiResponse(500, "error", err));
                else {
                    res.set("Access-Control-Expose-Headers", "*");
                    // res.set("Access-Control-Expose-Headers","X-Row-Header");                       
                    res.set("X-Paging-Header", Math.ceil(data.length / pagesize));
                    res.set("X-Row-Header", Math.ceil(data.length));
                    res.status(200).send(new apiResponse(200, "success", datafilter));
                }
            });
        }
    });
}


export async function getById(req, res) {
    try {
        const data = await adModel.findById(req.params.id).select(objectdisplay);
        console.log("data", data);
        res.status(200).send(new apiResponse(200, "success", data));
    } catch (err) {
        res.status(500).send(new apiResponse(500, "error", err));
    }
}

export async function updateById(req, res) {

    userModel.aggregate(builtQuery(req)).exec((err, data) => {
        if (err) res.status(500).send(new apiResponse(500, "error", err));

       
        req.body.receives = data.map(a => a._id);

        adModel.findByIdAndUpdate(req.params.id, req.body).then(data => {

            if (req.body.operate === '' || req.body.age == null) // don't target, so it is default => don't have receives field in db
            {
                //delete receives field in db.
                adModel.update({ _id: req.params.id }, { $unset: { receives: 1 } }).then(d => {
                    res.status(200).send(new apiResponse(200, "success", d));
                }).catch(err=>{
                    res.status(500).send(new apiResponse(500, "error", err));
                });

            }
            res.status(200).send(new apiResponse(200, "success", data));
        }).catch(err => {
            res.status(500).send(new apiResponse(500, "error", err));
        });


    });



}

export async function deleteById(req, res) {
    try {
        await adModel.findByIdAndDelete(req.params.id);
        res.status(200).send(new apiResponse(200, "success", {}));
    } catch (err) {
        res.status(500).send(new apiResponse(500, "error", err));
    }
}

export async function getImage(req, res) {
    try {

        const data = await adModel.findById(req.query.id);
        res.set('Content-Type', 'image/jpeg');
        res.status(200).send(new apiResponse(200, "success", data.image));
    } catch (err) {
        res.status(500).send(new apiResponse(500, "error", err));
    }
}


export async function upload(req, res) {
    try {

        let id = req.query.id;

        var upload = await AssetsService.uploadFile(req, ["advertise", id]);  //upload file to s3  
        const data = await adModel.findById(id);
        data.imageUrl = upload.filePath;
        console.log(data.imageUrl);
        await adModel.findOneAndUpdate({ _id: id }, data);
        res.status(200).send(new apiResponse(200, "success", {}));
    } catch (err) {
        res.status(500).send(new apiResponse(500, "error", err));
    }
} 