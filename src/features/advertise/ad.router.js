import express from "express";

import { create, getAll, getById, updateById, deleteById, paging, upload, getImage } from "./ad.controller";

export function advertiseRouter() {
  const router = express.Router();

  router.post("", create);
  router.get("", getAll);
  router.get("/page/:page/:pagesize/:sort", paging);
  router.get("/:id", getById);
  router.put("/:id", updateById);
  router.delete("/:id", deleteById);
  router.get("/image",getImage);
  router.post('/upload', upload);
  
  return router;
}