import mongoose from "mongoose";

const Schema = mongoose.Schema;

const ad = new Schema(
  {
    name: {
        type: String,      
        required: true,
    },
    imageUrl:{ 
      type: String,      
     // required: true,
    },
    image:{
      type: Buffer
    },
    linkUrl:{
      type: String,      
    //  required: true,
   },
    description: {
        type: String,      
        required: true,
    },
    receives: [],
    operate:String,
    age:Number,
    logic:String,
    zipcode:String,
    createdOn:Date
  }
);

// there is not receives if it is empty
ad.pre('save',function(next){
   if(this.receives.length===0)
   {
     console.log('save',this.receives);
       this.receives=undefined;
   }
    next();
})




export default ad;