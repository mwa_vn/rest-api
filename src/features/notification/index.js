import express from "express";
import NotificationService from "services/notification";

export function createNotificationRouter() {
  const router = express.Router();

  router.get('/view/:postId', (req, res, next) => {
    NotificationService.setNotificationRead(req.user.id, req.params.postId)
      .then(
        () => res.sendStatus(204),
        error => next(error)
      );
  });

  //to be used to test
  router.get("", (req, res, next) => {
    const notification = {
      title: 'Tien have notified a post for you!',
      body: 'Test Notification'
    };
    NotificationService.sendPostNotificationForFollower(['5e8b5cdb057141136b1d3ba2'], '6', notification)
      .then(
        result => res.send(result),
        error => next(error)
      );
  });

  //to be used to test
  router.get("/test-email", (req, res, next) => {
    NotificationService.sendEmailForUnhealthyPosts('trantienpham@gmail.com', 'Subject', 'Test Email')
      .then(
        result => res.send(result),
        error => next(error)
      );
  })

  return router;
}
