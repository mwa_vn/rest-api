import mongoose from "mongoose";

const Schema = mongoose.Schema;

const PostCommentSchema = new Schema(
  {
    message: {type: String, required: true},
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    likes: [{type: Schema.Types.ObjectId, ref: 'User'}],
  },
  { timestamps: true }
);

PostCommentSchema.method("toJSON", function() {
  const { __v, _id, user, likes, ...object } = this.toObject();
  object.id = _id;
  
  //author
  let avatar = 'https://i.pravatar.cc/300';
  if (user.userProfile.photo != undefined) avatar = user.userProfile.photo;
  object.user = { 
    id: user._id, 
    name: user.userProfile.firstname + ' ' + user.userProfile.lastname,  
    avatar: avatar};

  //list of like
  let likesObj = [];
  if (likes != null){
    likes.forEach(l => {
      likesObj.push({id: l._id, name: l.userProfile.firstname + ' ' + l.userProfile.lastname});
    });
  }
  object.likes = likesObj; 
  return object;
});

export default PostCommentSchema;
