import mongoose from "mongoose";
import userSchema from "./../users/user.model";
import postSchema from "./post.model";
import postCommentSchema from "./post_comment.model";
import NotificationService from "services/notification";
import { checkUnhealthy } from "./../profanity/profanity.controller";
import noop from 'lodash/fp/noop';
import has from 'lodash/fp/has';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/fp/get';

const User = mongoose.model("User", userSchema);
const Post = mongoose.model("Post", postSchema);
const PostComment = mongoose.model("PostComment", postCommentSchema);
const POST_LIMIT = 5;

export async function create(req, res) {
  try {
    let user = await User.findById(req.body.userId);
    if (user == undefined) throw Error('User is not found!');

    let post = new Post({
      message: req.body.message,
      user: user._id,
      images: req.body.images
    });
    post.unhealthy = await checkUnhealthy(req.body.message);
    // if post is unhealthy then disabled = true
    post.disabled = post.unhealthy;
    post = await post.save();
    if (post.unhealthy) {
      console.log('inc unhealthyPost, send notification or email to author, then he or she can post submit request to explain');
      let crrUser = await User.findByIdAndUpdate(req.user.id, {$inc: {unhealthyPost: 1}}, {new: true});
      if (crrUser.unhealthyPost >= 20) {
        console.log('User exceed limit 20 unhealthy post, disabled user, send notification, jwt token set invalid');
        await User.findByIdAndUpdate(req.user.id, {$set: {disabled: true}});

        //send email to user about his/her account deactivated
        const subject = 'Deactivated your account due you posted too much posts which contain unhealthy words';
        const content = 'Please email to admin to support your case.';
        NotificationService.sendEmailForUnhealthyPosts(req.user.username, subject, content)
          .catch(noop);
      }

      // notify for admin
      try {
        const fullName = `${get('userProfile.firstname')(user)} ${get('userProfile.lastname')(user)}`;
        await sendNotificationForAdmin(fullName, post.id);
      } catch {
        // ignore error if notification cannot be sent
      }
    } else if (req.body.isNotified) {
      try {
        const fullName = `${get('userProfile.firstname')(user)} ${get('userProfile.lastname')(user)}`;
        await sendNotificationForFollower(post.id, user._id, fullName);
      } catch {
        // ignore error if notification cannot be sent
      }
    }
   
    await Post.populate(post, { path: 'user' });
    res.send(post);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while creating the Post."
    });
  }
}

export async function getAll(req, res) {
  try {
    const criteria = {};
    if (has('unhealthy')(req.query)) {
      criteria.unhealthy = true;
    }
    const data = await Post.find(criteria)
      .populate({ path: 'user likes' })
      .populate({ path: 'comments', populate: { path: 'user likes' } })
      .sort('-createdAt');
    res.send(data);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving post."
    });
  }
}

export async function getById(req, res) {
  const id = req.params.postId;
  try {
    let post = await Post.findById(id)
      .populate({ path: 'user likes' })
      .populate({ path: 'comments', populate: { path: 'user likes' } });
    res.send(post);
  } catch (err) {
    res.status(500).send({ message: "Error retrieving Post with id=" + id });
  }
}

export async function remove(req, res) {
  const id = req.params.postId;
  try {
    let post = await Post.findById(id);
    await PostComment.remove({ _id: { $in: post.comments } });
    await post.remove();
    res.send({ message: 'Deleted post successfully' });
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while delete Post."
    });
  }

}

export async function getPostsByUser(req, res) {
  const { createdAt = '', search = '' } = req.query;
  
  try {
    let user = await User.findById(req.user.id);
    if (user == undefined) throw Error('User is not found!');
    
    // profile page of signedin user
    if (req.params.userId === user._id.toString()) {
      // all posts (do not care unhealthy, disabled)
      
      let match = { user: user._id};

      if (!isEmpty(createdAt)) {
        match.createdAt = { $lt: createdAt };
      }
      if (!isEmpty(search)) {
        match.message = { $regex: search };
      }

      let posts = await Post.find(match)
        .populate({ path: 'user likes' })
        .populate({ path: 'comments', populate: { path: 'user likes' } })
        .sort('-createdAt')
        .limit(POST_LIMIT);
      res.send(posts);

    } else { // profile page of other user 
      let isFollow = user.following.filter(e => e._id.toString() === req.params.userId).length != 0;
      if (isFollow) { // signed user follow this user
        
        let match = { user: req.params.userId, disabled: false};

        if (!isEmpty(createdAt)) {
          match.createdAt = { $lte: createdAt };
        }

        let posts = await Post.find(match)
        .populate({ path: 'user likes' })
        .populate({ path: 'comments', populate: { path: 'user likes' } })
        .sort('-createdAt')
        .limit(POST_LIMIT);
        res.send(posts);
      } else { // signed user does not follow this user
        // not follows req.params.userId so return []
        res.send([]);
      }
    }
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while query by User the Post."
    });
  }

}

export async function getFeedsByUser(req, res) {
  const { createdAt = '', search = '' } = req.query;
  try {
    let user = await User.findById(req.user.id);
    if (user == undefined) throw Error('User is not found!');

    let match = {
      disabled: false,
      $or: [
        { user: user._id },
        { user: { $in: user.following }}
      ]
    };

    if (!isEmpty(createdAt)) {
      match.createdAt = { $lt: createdAt };
    }
    if (!isEmpty(search)) {
      match.message = { $regex: search };
    }

    let posts = await Post.find(match)
      .populate({ path: 'user likes' })
      .populate({ path: 'comments', populate: { path: 'user likes' } })
      .sort('-createdAt')
      .limit(POST_LIMIT);
    res.send(posts);

  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while query by User the Post."
    });
  }
}

export async function likePost(req, res) {
  try {

    let user = await User.findById(req.body.userId);
    if (user == undefined) throw Error('User is not found!');

    let updateInfo = req.body.like ? { $addToSet: { likes: user._id } } : { $pull: { likes: user._id } };

    await Post.findByIdAndUpdate(req.params.postId, updateInfo);

    let updatedPost = await Post.findById(req.params.postId)
      .populate({ path: 'user likes' })
      .populate({ path: 'comments', populate: { path: 'user likes' } })

    res.send(updatedPost);

  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while like Post."
    });
  }
}

/*
export function setNotify(req, res) {
    
  const id = req.params.id;

  Post.findByIdAndUpdate(id, {isNotified: req.body.isNotified}, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot set notify Post with id=${id}. Maybe post was not found!`
        });
      } else res.send({ message: "Post was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Post with id=" + id
      });
    });    
}*/


export function changeStatus(req, res) {
  const id = req.params.postId;

  Post.findByIdAndUpdate(id, { disabled: req.body.disabled }, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot change status Post with id=${id}. Maybe post was not found!`
        });
      } else res.send({ message: "Post was updated successfully." });
    })
    .catch(() => {
      res.status(500).send({
        message: "Error updating Post with id=" + id
      });
    });
}

export async function addComment(req, res) {
  const id = req.params.postId;
  try {
    let user = await User.findById(req.body.userId);
    if (user == undefined) throw Error('User is not found!');

    let comment = await PostComment.create({
      message: req.body.message,
      user: user._id
    });

    await Post.findByIdAndUpdate(id, {
      $push:
        { comments: comment._id }
    })

    //await PostComment.populate(comment, { path: 'user' });

    // change flow data, return the whole post
    let updatedPost = await Post.findById(id)
      .populate({ path: 'user likes' })
      .populate({ path: 'comments', populate: { path: 'user likes' } })

    res.send(updatedPost);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while comment."
    });
  }
}

export async function likeComment(req, res) {
  const id = req.params.commentId;
  try {

    let user = await User.findById(req.body.userId);
    if (user == undefined) throw Error('User is not found!');

    let updateInfo = req.body.like ? { $addToSet: { likes: user._id } } : { $pull: { likes: user._id } };

    await PostComment.findByIdAndUpdate(id, updateInfo);

    // change flow data, return the whole post
    let updatedPost = await Post.findById(req.body.postId)
      .populate({ path: 'user likes' })
      .populate({ path: 'comments', populate: { path: 'user likes' } })

    res.send(updatedPost);

  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while like comment."
    });
  }
}

export async function removeComment(req, res) {
  const id = req.params.commentId;
  try {

    let p = await Post.findOneAndUpdate({ comments: { "$in": [id] } },
      { $pull: { comments: id } });
    await PostComment.findByIdAndRemove(id);

    // change flow data, return the whole post
    let updatedPost = await Post.findById(p._id)
      .populate({ path: 'user likes' })
      .populate({ path: 'comments', populate: { path: 'user likes' } })

    res.send(updatedPost);
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while like comment."
    });
  }
}

//private
async function sendNotificationForFollower(postId, userId, userName) {
  const followers = await User.find({following: {$in: [userId]}});
  const followerIds = followers.map(f => f.id);
  const notification = {
    title: `${userName} have notified a post for you!`,
    body: `${userName} have notified a post for you!`
  };
  return NotificationService.sendPostNotificationForFollower(followerIds, postId, notification);
}
async function sendNotificationForAdmin(userName, postId) {
  const admins = await User.find({role: {$eq: 'ADMIN'}});
  const adminIds = admins.map(f => f.id);
  const notification = {
    title: `${userName} have posted an unhealthy post!`,
    body: `${userName} have posted an unhealthy post!`
  };
  return NotificationService.sendUnhealthyNotificationForAdmin(adminIds, postId, notification);
}