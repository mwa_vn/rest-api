import mongoose from "mongoose";

const Schema = mongoose.Schema;

const PostSchema = new Schema(
  {
    message: {type: String, required: true},
    images: [{type: String}],
    unhealthy: {type: Boolean, required: true, default: false},
    disabled: {type: Boolean, default: false},
    user: {type: Schema.Types.ObjectId, ref: 'User'},
    likes: [{type: Schema.Types.ObjectId, ref: 'User'}],
    comments: [{type: Schema.Types.ObjectId, ref: 'PostComment'}],
  },
  { timestamps: true }
);

PostSchema.method("toJSON", function() {
  const { __v, _id, user, likes, comments, ...object } = this.toObject();
  object.id = _id;
  //author
  let avatar = 'https://i.pravatar.cc/300';
  if (user.userProfile.photo != undefined) {
    avatar = user.userProfile.photo;
  }
  object.user = { 
    id: user._id, 
    name: user.userProfile.firstname + ' ' + user.userProfile.lastname, 
    avatar: avatar};

  //list of like
  let likesObj = [];
  if (likes != null){
    likes.forEach(l => {
      likesObj.push({id: l._id, name: l.userProfile.firstname + ' ' + l.userProfile.lastname});
    });
  }
  object.likes = likesObj; 

  //list of comments
  let cmtsObj = [];
  if (comments != null){
    comments.forEach(c => {
      let { _id, userProfile} = c.user;
      let avatar = userProfile.photo || 'https://i.pravatar.cc/300';
      let cmtLikes = [];
      if (c.likes != null) {
        c.likes.forEach(ck => {
          cmtLikes.push({id: ck._id, name: ck.userProfile.firstname + ' ' + ck.userProfile.lastname});
        })
      }
      cmtsObj.push({id: c._id, createdAt: c.createdAt, message: c.message, 
        user: { id: _id, name: userProfile.firstname + ' ' + userProfile.lastname, 
          avatar: avatar}, likes: cmtLikes});
      
    });
  }
  object.comments = cmtsObj;

  return object;
});

export default PostSchema;
