import express from "express";
import PostsService from "services/posts";
import { create, getAll, getById, remove, getPostsByUser, getFeedsByUser, likePost, addComment, removeComment, likeComment, changeStatus } from "./post.controller";

export function createPostRoute() {
  const router = express.Router();

  router.post("/:id/upload-images", (req, res, next) => {
    PostsService.uploadImage(req)
      .then(fileData => res.json(fileData))
      .catch(error => {
        console.log(error);
        next(error);
      });
  });

  router.post("", create);
  router.get("", getAll);
  router.get("/feeds", getFeedsByUser);
  router.get("/:postId", getById);
  router.delete("/:postId", remove);
  router.get("/user/:userId", getPostsByUser);
  // {"unhealthy": true|false}
  router.put("/:postId/changePublishStatus", changeStatus);
  // {"userId": "xxx", "like": true | false}
  router.put("/:postId/like", likePost);
  // {"userId": "xxx", "message": "what is it?"}
  router.put("/:postId/comments", addComment);
  // {"userId": "xxx", "like": true | false}
  router.put("/comments/:commentId/like", likeComment);
  router.delete("/comments/:commentId", removeComment);

  return router;
}
