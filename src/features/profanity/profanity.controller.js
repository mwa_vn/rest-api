import mongoose from "mongoose";
import Filter from "bad-words";
import profanitySchema from "./../profanity/profanity.model";

const Profanity = mongoose.model("Profanity", profanitySchema);
let filter = new Filter({emptyList: true});

export async function create(req, res) {
    const { language, listwords, enabled } = req.body;
    try {
        let words =  listwords.split(",").map( item => item.trim());
        const data = await new Profanity({language, words, enabled}).save();
        res.json(data);
    } catch (err) {
        console.log(err);
        res.json({message: err});
    }
};

export async function getAll(req, res) {
    try {
        const data = await Profanity.find({});
        res.json(data);
    } catch (err) {
        console.log(err);
        res.json({message: err});
    }
}

export async function getById(req, res) {
    try {
        const data = await Profanity.findById(req.params.id);
        res.json(data);
    } catch (err) {
        console.log(err);
        res.json({message: err});
    }
}

export async function updateById(req, res) {
    const { language, listwords, enabled } = req.body;
    try {
        let words =  listwords.split(",").map( item => item.trim());
        await Profanity.findByIdAndUpdate(req.params.id, {language, words, enabled});
        res.json({message: 'update done'});
    } catch (err) {
        console.log(err);
        res.json({message: err});
    }
}

export async function deleteById(req, res) {
    try {
        await Profanity.findByIdAndDelete(req.params.id);
        res.json({message: 'delete done'});
    } catch (err) {
        console.log(err);
        res.json({message: err});
    }
}

export async function addWord(req, res) {
    try {
        const p = await Profanity.findById(req.params.id);
        p.words.addToSet(req.body.word);
        await p.save();
        res.json({message: 'add word done'});
    } catch (err) {
        console.log(err);
        res.json({message: err});
    }
}

export async function deleteWord(req, res) {
    try {
        const p = await Profanity.findById(req.params.id);
        p.words.pull(req.body.word);
        await p.save();
        res.json({message: 'delete word done'});
    } catch (err) {
        console.log(err);
        res.json({message: err});
    }
}

export async function syncFilter(req, res) {
    try {
        filter.list = [];
        const list = await Profanity.find({enabled: true});
        for (let i = 0; i< list.length; i++) {
            filter.addWords(...list[i].words);
        }
        res.json({message: 'Profanity: ' + filter.list.length + ' words'});
    } catch (err) {
        console.log('err', err);
        res.status(500).json({message: err});
    }
}


export async function checkProfanity(req, res) {
    try {
        const valid = filter.isProfane(req.body.text);
        let result = {profanity: valid, text: req.body.text};
        if (valid) {
            result.clean = filter.clean(req.body.text)
        }
        res.send(result);
    } catch (err) {
        res.send({error: err});
    }
}

export async function checkUnhealthy(text) {
    let filter = new Filter({emptyList: true});
    try {
        const list = await Profanity.find({enabled: true});
        for (let i = 0; i< list.length; i++) {
            filter.addWords(...list[i].words);
        }
        return filter.isProfane(text);
    } catch (err) {
        console.log('err', err);
    }
    return false;
}