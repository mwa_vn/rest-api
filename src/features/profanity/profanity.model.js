import mongoose from "mongoose";

const Schema = mongoose.Schema;

const ProfanitySchema = new Schema(
  {
    language: {
        type: String,
        trim: true,
        required: true,
    },
    enabled: {type: Boolean, required: true, default: true},
    words: [String]
  },
  { timestamps: true }
);

export default ProfanitySchema;