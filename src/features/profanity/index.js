import express from "express";
import { create, getAll, getById, updateById, deleteById, addWord, deleteWord, checkProfanity, syncFilter } from "./profanity.controller";

export function createProfanityRouter() {
  const router = express.Router();

  router.post("", create);
  router.get("", getAll);

  router.get("/sync", syncFilter);
  router.post("/check", checkProfanity);
  router.get("/:id", getById);
  router.put("/:id", updateById);
  router.delete("/:id", deleteById);
  router.post("/:id/add-word", addWord);
  router.post("/:id/delete-word", deleteWord);
  

  return router;
}