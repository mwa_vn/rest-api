import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import compression from "compression";
import { apiRoutes } from "./routes";
import { authenticatedMiddleware } from "features/users/user.controller";

const app = express();

app.use(compression());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(authenticatedMiddleware());
app.use("/api", apiRoutes());

function getResponse (message) {
  return {
    error: message
  };
}
function handleGlobalError(err, req, res, next) {
	if (err && err.name === 'UnauthorizedError') {
		console.error(err.stack);//this error will be logged into cloudwatch if it's run on cloud otherwise will be printed in console
		res.status(401).send(getResponse(err.message));
	} else if (err) {
		console.error(err.stack);//this error will be logged into cloudwatch if it's run on cloud otherwise will be printed in console
		res.status(500).send(getResponse(err.message));
	} else {
		next();
	}
}
app.use(handleGlobalError)

module.exports = app;
