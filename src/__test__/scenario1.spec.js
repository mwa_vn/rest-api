import supertest from "supertest";
import app from "../app.dev";
import mongoose from "mongoose";
import userSchema from "./../features/users/user.model";
import postSchema from "./../features/posts/post.model";
import {users, posts} from "./data";

const User = mongoose.model("User", userSchema);
const Post = mongoose.model("Post", postSchema);

beforeAll(async () => {
    mongoose.Promise = global.Promise;
    await mongoose.connect('mongodb://localhost/dbmanning', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database test!");
    })
    .catch(err => {
        console.log("Cannot connect to the database test!", err);
        process.exit();
    });

    let user = new User({
        email: "admin@mail.com", 
        zipcode: 52556, 
        dob: "2000-01-01",
        userProfile: {
            firstname: "Admin",
            lastname: "Nguyen",
            gender: "male"
        }
    });
    user.setPassword("admin");
    await user.save();
});

afterAll(async () => {
    await User.deleteMany();
    await Post.deleteMany();
    await mongoose.connection.close();
});

let thaoToken = '', thaoId = '';
let bobToken = '', bobId = '';
let aliceToken = '', aliceId = '';

describe("Testing user api", () => {

    test("tests registers", async () => {
        for (let i = 0; i< users.length; i++) {
            await testHelper.verifyRegister(users[i]);
        }
    });

    test("tests logins", async () => {
        const rs = await testHelper.verifyAuthenticate(users[0]);
        thaoToken = rs.token;
        thaoId = rs.uid;

        const rs2 = await testHelper.verifyAuthenticate(users[1])
        bobToken = rs2.token;
        bobId = rs2.uid;

        const rs3 = await testHelper.verifyAuthenticate(users[2])
        aliceToken = rs3.token;
        aliceId = rs3.uid;
    });
});

describe("Testing post api", () => {
    test("create posts", async () => {
        for (let i = 0; i< posts.thao.length; i++) {
            await testHelper.verifyPost(thaoToken, thaoId, posts.thao[i],
                 'Thao Nguyen', false, false);
        }
        for (let i = 0; i< posts.bob.length; i++) {
            await testHelper.verifyPost(bobToken, bobId, posts.bob[i],
                'Bob booooob', false, false);
        }
        for (let i = 0; i< posts.alice.length; i++) {
            await testHelper.verifyPost(aliceToken, aliceId, posts.alice[i],
                'Alice Forest', false, false);
        }
    });
});

describe("Testing follow api", () => {

    test("tests follows", async () => {
        await testHelper.verifyFollow(thaoToken, bobId, true);
        await testHelper.verifyFollow(thaoToken, aliceId, true);
        await testHelper.verifyFollow(bobToken, thaoId, true);
        await testHelper.verifyFollow(aliceToken, thaoId, true);
    });

    test("tests get my followers and following", async () => {
        
    });

});

describe("Testing get posts api", () => {

    test("tests get feed", async () => {
        
    });

    test("tests get my posts", async () => {
        
    });

    test("tests get posts of person who I followed", async () => {
        
    });

    test("tests get posts of person who I have not followed", async () => {
        
    });

});

const testHelper = {
    verifyFollow: async (usertoken, personid, stt) => {
        let res = await supertest(app).put(`/api/users/follow/${personid}/${stt}`)
                .set({ Authorization: 'Bearer ' + usertoken, Accept: 'application/json' })
                .send({});
            expect(res.status).toBe(200);
            expect(res.body.message).toEqual('(Un)follow is successfully');
    },
    verifyPost: async (usertoken, userid, post, authorname, unhealthy, disabled) => {
        const response = await supertest(app).post('/api/posts')
                .set({ Authorization: 'Bearer ' + usertoken, Accept: 'application/json' })
                .send({userId: userid, ...post});
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('id');
            expect(response.body.user).toHaveProperty('id');
            expect(response.body.user).toHaveProperty('avatar');
            expect(response.body.user.name).toEqual(authorname);
            expect(response.body.unhealthy).toEqual(unhealthy);
            expect(response.body.disabled).toEqual(disabled);
    }, 
    verifyAuthenticate: async (user) => {
        const response = await supertest(app).post('/api/users/authenticate')
            .send({username: user.email, password: user.password});
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('token');
        expect(response.body.username).toBe(user.email);
        return {token: response.body.token, uid: response.body.id};
    },
    verifyRegister: async (user) => {
        const response = await supertest(app).post('/api/users/register')
            .send(user);
            expect(response.status).toBe(200);
            expect(response.body).toHaveProperty('id');
    },
}

