const users = [
    {
        email: "thaox@mail.com", 
        password: "thaox", 
        zipcode: 52556, 
        dob: "2000-01-01",
        userProfile: {
            firstname: "Thao",
            lastname: "Nguyen",
            gender: "male"
        }
    },
    {
        email: "bob@mail.com", 
        password: "bob", 
        zipcode: 52556, 
        dob: "2000-01-01",
        userProfile: {
            firstname: "Bob",
            lastname: "booooob",
            gender: "male"
        }
    },
    {
        email: "alice@mail.com", 
        password: "alice", 
        zipcode: 52556, 
        dob: "2000-01-01",
        userProfile: {
            firstname: "Alice",
            lastname: "Forest",
            gender: "female"
        }
    }
];

const posts = {
    thao: [
        {
            message: "thao message 1",
            images: [
                "https://via.placeholder.com/150",
                "https://via.placeholder.com/250"
            ]
        },
        {
            message: "thao message 2",
            images: [
                "https://via.placeholder.com/150"
            ]
        },
        {
            message: "thao message 3",
            images: [
                "https://via.placeholder.com/150"
            ]
        }
    ],
    bob: [
        {
            message: "bob message 1",
            images: [
                "https://via.placeholder.com/150",
                "https://via.placeholder.com/250"
            ]
        }
    ],
    alice: [
        {
            message: "alice message 1",
            images: [
                "https://via.placeholder.com/150",
                "https://via.placeholder.com/250"
            ]
        }
    ]
}

module.exports =  {
    users, posts
}; 