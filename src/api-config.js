import { config } from "dotenv";

config();
export default {
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
  clientId: process.env.IDENTITY_CLIENT_ID,
  clientSecret: process.env.IDENTITY_CLIENT_SECRET,
  assetsBucket: process.env.ASSETS_BUCKET,
  domainAssets: process.env.ASSETS_BASE_URL,

  //paths for each features like posts
  postsPath: process.env.POSTS_PATH,

  mongoUrl: process.env.MONGO_DB_CONNECTION,
  jwtExpiresIn: 365,
  jwtSecretKey: 'mwa_vn_@123456?'
};
