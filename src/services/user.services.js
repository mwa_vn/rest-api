import AssetsService from "./assets";
import settings from "api-config";

class UserService {
    uploadImage(req) {
        const useId = req.params.id;
        return AssetsService.uploadFile(req, [settings.userProfilePath, useId]);
    }
}

export default new UserService();