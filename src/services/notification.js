import * as admin from 'firebase-admin';
import map from "lodash/map";
import isEmpty from "lodash/isEmpty";

const serviceAccount = require('../../firebase-service-account.json')
const firebase = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://mwa-vn.firebaseio.com'
});

export class NotificationService {
  sendPostNotificationForFollower(followerIds, postId, notification) {
    if (isEmpty(followerIds)) return Promise.resolve(true);

    const requests = map(followerIds, receiverId => {
      return firebase.database().ref(`notifications/${receiverId}/${postId}`).set({
        notification,
        timestamp: admin.database.ServerValue.TIMESTAMP,
        unread: 1
      });
    });
    return Promise.all(requests);
  }
  sendUnhealthyNotificationForAdmin(adminIds, postId, notification) {
    if (isEmpty(adminIds)) return Promise.resolve(true);
    const requests = map(adminIds, adminId => {
      return firebase.database().ref(`notifications/${adminId}/${postId}`).set({
        notification,
        timestamp: admin.database.ServerValue.TIMESTAMP,
        unread: 1,
        unhealthy: true
      });
    });
    return Promise.all(requests);
  }
  sendActiveRequestNotificationForAdmin(adminIds, notification, userId) {
    if (isEmpty(adminIds)) return Promise.resolve(true);
    const requests = map(adminIds, adminId => {
      return firebase.database().ref(`notifications/${adminId}/${userId}`).set({
        notification,
        timestamp: admin.database.ServerValue.TIMESTAMP,
        unread: 1,
        activeRequest: true
      });
    });
    return Promise.all(requests);
  }
  sendEmailForUnhealthyPosts(to, subject, content) {
    return firebase.database().ref(`emails/unhealthy-posts`).push({
      to,
      subject,
      content,
      timestamp: admin.database.ServerValue.TIMESTAMP
    });
  }
  setNotificationRead(userId, postId) {
    return firebase.database().ref(`notifications/${userId}/${postId}`).update({
      unread: 0
    })
  }
}

export default new NotificationService();