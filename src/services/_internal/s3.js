import AWS from "aws-sdk";
import utils from "utils";
import fs from "fs";
import formidable from "formidable";
import apiConfig from "api-config";
import join from "lodash/join";
import concat from "lodash/concat";
import isEmpty from "lodash/isEmpty";
import get from "lodash/fp/get";

const BUCKET = apiConfig.assetsBucket;
const s3 = new AWS.S3();

const upload = (file_name, file, contentType = "image/jpg") => {
  const params = {
    Bucket: BUCKET,
    Key: file_name,
    Body: file,
    ACL: "public-read",
    ContentType: contentType
  };
  return new Promise((resolve, reject) => {
    s3.putObject(params, (err, resp) => {
      if (err) {
        reject({ success: false, data: err });
      }
      resolve({ sucess: true, data: resp });
    });
  });
};

class S3Service {
  getFileData(path, fileName) {
    const filePath = `${path}/${fileName}`;

    return new Promise((resolve, reject) => {
      s3.headObject({ Key: filePath, Bucket: BUCKET }, (err, data) => {
        if (err) {
          return reject(err);
        }

        return resolve({
          file: fileName,
          size: data.ContentLength,
          modified: data.LastModified
        });
      });
    });
  }

  getFilesData(files) {
    return files
      .map(fileName => this.getFileData(fileName))
      .filter(fileData => fileData !== null)
      .sort((a, b) => a.modified - b.modified);
  }

  getFiles(path) {
    return new Promise((resolve, reject) => {
      s3.ListObjects({ Key: path, Bucket: BUCKET }, (err, data) => {
        if (err) {
          return reject(err);
        }
        const filesData = data.Contents.map(ObjectData => ({
          file: ObjectData.Key,
          size: data.Size,
          modified: ObjectData.LastModified
        }));
        resolve(filesData);
      });
    });
  }

  deleteFile(path, fileName) {
    const params = {
      Bucket: BUCKET,
      Delete: {
        Objects: [
          {
            Key: `${path}/${fileName}`
          }
        ]
      }
    };
    return new Promise((resolve, reject) => {
      s3.deleteObject(params, (err, data) => {
        if (err) {
          return reject("File not found");
        }
        resolve(data);
      });
    });
  }

  deleteDir(path) {
    return this.emptyDir(path);
  }

  emptyDir(path) {
    let params = {
      Bucket: BUCKET,
      Prefix: path
    };
    return new Promise((resolve, reject) => {
      s3.listObjects(params, (err, data) => {
        if (err) return;

        if (data.Contents.length == 0) return;

        params = { Bucket: BUCKET };
        params.Delete = { Objects: [] };

        data.Contents.forEach(content => {
          params.Delete.Objects.push({ Key: content.Key });
        });

        s3.deleteObjects(params, (error, output) =>
          error ? reject(error) : resolve(output)
        );
      });
    });
  }

  uploadFile(req, paths) {
    const form = new formidable.IncomingForm();
    let file_name = null;
    let $file = null;

    return new Promise((resolve, reject) => {
      form
        .on("fileBegin", (name, file) => {
          // Emitted whenever a field / value pair has been received.
          file.name = utils.getCorrectFileName(file.name);
        })
        .on("file", (name, file) => {
          // every time a file has been uploaded successfully,
          file_name = file.name;
          $file = file;
        })
        .on("error", err => {
          reject(this.getErrorMessage(err));
        })
        .on("end", () => {
          const size = get('size')($file);
          const filePath = join(concat(paths, file_name), "/");
          if (isNaN(size)) return resolve({ error: 'There is an error during processing in server.' });
          if ((size / (1000 * 1000)) > 1) return resolve({ error: 'Your file is greater than 1 MB' });
          upload(filePath, fs.createReadStream($file.path), $file.type)
            .then(fileData =>
              resolve({
                ...fileData,
                filePath: `${apiConfig.domainAssets}/${filePath}`
              })
            )
            .catch(err => reject(err));
        });

      form.parse(req);
    });
  }

  uploadFiles(req, paths) {
    const uploadedFiles = [];
    const errors = [];

    const form = new formidable.IncomingForm({ multiples: true });
    return new Promise((resolve, reject) => {
      form
        .on("fileBegin", (name, file) => {
          // Emitted whenever a field / value pair has been received.
          file.name = utils.getCorrectFileName(file.name);
        })
        .on("file", async (field, file) => {
          // every time a file has been uploaded successfully,
          if (file.name) {
            const filePath = join(join(concat(paths, file.name), "/"));
            upload(filePath, fs.createReadStream(file.path), file.type)
              .then(() => uploadedFiles.push(filePath))
              .catch(err => errors.push(err));
          }
        })
        .on("error", err => errors.push(this.getErrorMessage(err)))
        .on("end", () => {
          if (!isEmpty(errors)) return reject(errors);
          return resolve(uploadedFiles);
        });

      form.parse(req);
    });
  }

  getErrorMessage(err) {
    return { error: true, message: err.toString() };
  }
}

export default new S3Service();
