import S3Service from "./_internal/s3";

class AssetsService {
  getFileData(path, fileName) {
    return S3Service.getFileData(path, fileName);
  }

  getFilesData(path, files) {
    return S3Service.getFilesData(path, files);
  }

  getFiles(path) {
    return S3Service.getFiles(path);
  }

  deleteFile(path, fileName) {
    return S3Service.deleteFile(path, fileName);
  }

  deleteDir(path) {
    return S3Service.deleteDir(path);
  }

  emptyDir(path) {
    return S3Service.emptyDir(path);
  }

  uploadFile(req, path) {
    return S3Service.uploadFile(req, path);
  }

  uploadFiles(req, res, path, onFileUpload, onFilesEnd) {
    return S3Service.uploadFiles(req, res, path, onFileUpload, onFilesEnd);
  }
}

export default new AssetsService();
