import AssetsService from "./assets";
import settings from "api-config";

class PostService {
  uploadImage(req) {
    const postId = req.params.id;
    return AssetsService.uploadFile(req, [settings.postsPath, postId]);
  }
}

export default new PostService();
